import sys
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QMessageBox, QTableWidgetItem
import mainwindow_view
from algorithm_proxy import MLAlgorithmsProxy

class MainApp(QMainWindow, mainwindow_view.Ui_MainWindow):
	def __init__(self, parent=None):
		super(MainApp, self).__init__(parent)
		self.setupUi(self)

		self.files_model = QStandardItemModel()
		self.files_list.setModel(self.files_model)
		self.files_selected_model = self.files_list.selectionModel()

		self.table_result_model = QStandardItemModel()
		self.table_result_model.setHorizontalHeaderLabels(['Word', 'Weight'])
		self.table_result_view.setModel(self.table_result_model)

		self.ml_proxy = MLAlgorithmsProxy()

		self.openfile_btn.clicked.connect(self.open_files)
		self.analyze_btn.clicked.connect(self.generate_weights)
		self.exportXML_btn.clicked.connect(self.export_to_XML)
		self.delete_btn.clicked.connect(self.delete_selected)

	def confirm_delete(self):
		sure_msg = "Are you sure you want to delete selected entries?"
		reply = QMessageBox.question(self, 'Delete?', sure_msg, QMessageBox.Yes, QMessageBox.No)
		return reply

	def delete_selected(self):
		selected = self.files_selected_model.selectedRows()
		if len(selected) > 0:
			if self.confirm_delete() == QMessageBox.Yes:
				for sel in selected:
					self.files_model.removeRow(sel.row())
		else:
			pass

	def open_files(self):
		options = QFileDialog.Options()
		options |= QFileDialog.ReadOnly
		files, _ = QFileDialog.getOpenFileNames(self, "Open files", "","Text files (*.txt);;All Files (*)", options=options)
		for file in files:
			item = QStandardItem(file)
			item.setCheckable(False)
			self.files_model.appendRow(item)

	def generate_weights(self):
		if not self.files_model.rowCount() > 0:
			return

		while not self.table_result_model.rowCount() == 0:
			self.table_result_model.removeRow(0);

		files = []
		for row in range(self.files_model.rowCount()):
			index = self.files_model.index(row, 0)
			files.append(self.files_model.data(index))
		res = self.ml_proxy.calculate_weights(files)

		row = 0
		for key, value in res.items():
			self.table_result_model.setItem(row , 0, QStandardItem(key))
			self.table_result_model.setItem(row , 1, QStandardItem(str(value)))
			row += 1

	def export_to_XML(self):
		return NotImplemented

	def export_to_CSV(self):
		return NotImplemented
