# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Vitaliy Acunin\Desktop\pymorphy2 project\mainwindow_view.ui'
#
# Created by: PyQt5 UI code generator 5.6.dev1604041221
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(398, 387)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.top_layout = QtWidgets.QHBoxLayout()
        self.top_layout.setObjectName("top_layout")
        self.openfile_btn = QtWidgets.QPushButton(self.centralwidget)
        self.openfile_btn.setObjectName("openfile_btn")
        self.top_layout.addWidget(self.openfile_btn)
        self.delete_btn = QtWidgets.QPushButton(self.centralwidget)
        self.delete_btn.setObjectName("delete_btn")
        self.top_layout.addWidget(self.delete_btn)
        self.verticalLayout.addLayout(self.top_layout)
        self.files_list = QtWidgets.QListView(self.centralwidget)
        self.files_list.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.files_list.setSelectionRectVisible(True)
        self.files_list.setObjectName("files_list")
        self.verticalLayout.addWidget(self.files_list)
        self.table_result_view = QtWidgets.QTableView(self.centralwidget)
        self.table_result_view.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.table_result_view.setObjectName("table_result_view")
        self.verticalLayout.addWidget(self.table_result_view)
        self.bot_layout = QtWidgets.QHBoxLayout()
        self.bot_layout.setObjectName("bot_layout")
        self.analyze_btn = QtWidgets.QPushButton(self.centralwidget)
        self.analyze_btn.setObjectName("analyze_btn")
        self.bot_layout.addWidget(self.analyze_btn)
        self.exportXML_btn = QtWidgets.QPushButton(self.centralwidget)
        self.exportXML_btn.setObjectName("exportXML_btn")
        self.bot_layout.addWidget(self.exportXML_btn)
        self.verticalLayout.addLayout(self.bot_layout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.openfile_btn.setText(_translate("MainWindow", "Open file(s)"))
        self.delete_btn.setText(_translate("MainWindow", "Delete selected"))
        self.analyze_btn.setText(_translate("MainWindow", "Generate weights"))
        self.exportXML_btn.setText(_translate("MainWindow", "Export to XML file"))

