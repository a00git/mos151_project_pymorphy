import re, pymorphy2
from sklearn.feature_extraction.text import TfidfVectorizer

class MLAlgorithmsProxy():
	vectorizer = None

	def __init__(self):
		__class__.vectorizer = TfidfVectorizer(
			analyzer = 'word',
			input = 'filename',
			decode_error = 'ignore',
			tokenizer = self._tokenizer,
			preprocessor = self._preprocessor,
			smooth_idf = False
		)

	@staticmethod
	def _tokenizer(s):
		morph = pymorphy2.MorphAnalyzer()
		tokens = []

		if s is not None:
			re.sub('[^A-Za-z0-9]+', '', s)
			items = s.split(' ')

			for item in items:
				if len(item) > 0:
					word = morph.parse(item.replace('.',''))[0]
					if word.tag.POS not in ('NUMR', 'PREP', 'CONJ', 'PRCL', 'INTJ'):
						tokens.append( word.normal_form)

		return tokens

	@staticmethod
	def _preprocessor(s):
		if s is not None and re.match('[а-я]+', s.lower()) and len(s) > 4:
			return s.lower()
		else:
			return ' '
		
	@classmethod
	def calculate_weights(cls, files):
		no_dups = list(set(files))

		term_matrix = cls.vectorizer.fit_transform(no_dups) # Tf-idf-weighted document-term matrix
		idf = cls.vectorizer.idf_ # learned idf vector (global term weights) 
		return dict(zip(cls.vectorizer.get_feature_names(), idf))
